<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Request\CategoryRequest;
use App\Services\CategoryServiceImplementation;

class CategoryController extends Controller
{
    public function __construct(private readonly CategoryServiceImplementation $categoryServiceImplementation)
    {
    }

    public function index()
    {
        $category = $this->categoryServiceImplementation->index();

        return CategoryRequest::collection($category);
    }

    public function store(CategoryRequest $request)
    {
        $categoryStore = $this->categoryServiceImplementation->store($request);

        return CategoryRequest::make($categoryStore);
    }

    public function show(Category $category)
    {
        $category = $this->categoryServiceImplementation->show($category);

        return CategoryRequest::make($category);
    }

    public function update(CategoryRequest $request, Category $category)
    {
        $categoryUpdate = $this->categoryServiceImplementation->update($request, $category);

        return CategoryRequest::make($categoryUpdate);
    }

    public function destroy(Category $category)
    {
        $this->categoryServiceImplementation->destroy($category);
    }
}
