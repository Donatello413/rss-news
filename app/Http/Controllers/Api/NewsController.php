<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Request\GetNewsWithParamRequest;
use App\Request\NewsRequest;
use App\Resources\NewsResource;
use App\Services\NewsServiceImplementation;

class NewsController extends Controller
{
    public function __construct(private readonly NewsServiceImplementation $newsServiceImplementation)
    {
    }

    public function index()
    {
        $news = $this->newsServiceImplementation->index();

        return NewsResource::collection($news);
    }

    public function store(NewsRequest $request)
    {
        $newsStore = $this->newsServiceImplementation->store($request);

        return NewsResource::make($newsStore);
    }

    public function show(News $news)
    {
        $news = $this->newsServiceImplementation->show($news);

        return NewsResource::make($news);
    }

    public function update(NewsRequest $request, News $news)
    {
        $newsUpdate = $this->newsServiceImplementation->update($request, $news);

        return NewsResource::make($newsUpdate);
    }

    public function destroy(News $news)
    {
        $this->newsServiceImplementation->destroy($news);
    }

    public function getNewsInThePeriodWithCategoryAndSource(GetNewsWithParamRequest $request)
    {
        $news = $this->newsServiceImplementation->getNewsInThePeriodWithCategoryAndSource($request);

        return NewsResource::collection($news);
    }
}
