<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Source;
use App\Request\GetSourceWithParamRequest;
use App\Request\SourceRequest;
use App\Resources\SourceResource;
use App\Services\SourceServiceImplementation;

class SourceController extends Controller
{
    public function __construct(private readonly SourceServiceImplementation $sourceServiceImplementation)
    {
    }

    public function index()
    {
        $source = $this->sourceServiceImplementation->index();

        return SourceResource::collection($source);
    }

    public function store(SourceRequest $request)
    {
        $sourceStore = $this->sourceServiceImplementation->store($request);

        return SourceResource::make($sourceStore);
    }

    public function show(Source $source)
    {
        $source = $this->sourceServiceImplementation->show($source);

        return SourceResource::make($source);
    }

    public function update(SourceRequest $request, Source $source)
    {
        $sourceUpdate = $this->sourceServiceImplementation->update($request, $source);

        return SourceResource::make($sourceUpdate);
    }

    public function destroy(Source $source)
    {
        $this->sourceServiceImplementation->destroy($source);
    }

    public function getSourceWithCountNewsInThePeriod(GetSourceWithParamRequest $request)
    {
        $source = $this->sourceServiceImplementation->getSourceWithCountNewsInThePeriod($request);

        return SourceResource::collection($source);
    }
}
