<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\News;
use Illuminate\Support\Facades\Http;

class FetchNewsCommand extends Command
{
    protected $signature = 'fetch:news';

    protected $description = 'Fetch news from sources';

    public function handle()
    {
        $sources = [
            ['name' => 'РИА Новости', 'url' => 'https://ria.ru/export/rss2/archive/index.xml'],
            ['name' => 'Известия', 'url' => 'https://iz.ru/xml/rss/all.xml'],
            ['name' => 'Лента', 'url' => 'https://lenta.ru/rss'],
            ['name' => 'РБК', 'url' => 'https://rssexport.rbc.ru/rbcnews/news/30/full.rss'],
        ];

        foreach ($sources as $source) {

            $rss = Http::get($source['url']);

            $xml = new SimpleXMLElement($rss->body());

            foreach ($xml->channel->item as $item) {
                $news = new News([
                    'title' => $item->title,
                    'description' => $item->description,
                    'link' => $item->link,
                    'publication_date' => date('Y-m-d H:i:s', strtotime($item->pubDate)),
                    'source_id' => '',
                    'category_id' => '',
                    'image_url' => (string)$item->enclosure['url'] ?? null,
                ]);

                $news->save();
            }
        }

        $this->info('News fetched and saved successfully.');
    }
}
