<?php

namespace App\Resources;

use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property-read News $resource
 */
class NewsResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'title' => $this->resource->title,
            'description' => $this->resource->description,
            'link' => $this->resource->link,
            'publication_date' => $this->resource->publication_date,
            'source' => $this->resource->source->title,
            'category' => $this->resource->category->name,
            'image_url' => $this->resource->image_url,
        ];
    }
}
