<?php

namespace App\Services;

use App\Models\Category;
use App\Request\CategoryRequest;

class CategoryServiceImplementation
{
    public function index()
    {
        return Category::all();
    }

    public function store(CategoryRequest $request)
    {
        $validatedData = $request->validated();

        return Category::create($validatedData);
    }

    public function show(Category $category)
    {
        return Category::query()->findOrFail($category->id);
    }

    public function update(CategoryRequest $request, Category $category)
    {
        $validatedData = $request->validated();

        return $category->update($validatedData);
    }

    public function destroy(Category $category)
    {
        return $category->delete();
    }
}
