<?php

namespace App\Services;

use App\Models\News;
use App\Request\GetNewsWithParamRequest;
use App\Request\NewsRequest;

class NewsServiceImplementation
{
    public function index()
    {
        return News::all();
    }

    public function store(NewsRequest $request)
    {
        $validatedData = $request->validated();

        return News::create($validatedData);
    }

    public function show(News $news)
    {
        return News::query()->findOrFail($news->id);
    }

    public function update(NewsRequest $request, News $news)
    {
        $validatedData = $request->validated();

        return $news->update($validatedData);
    }

    public function destroy(News $news)
    {
        return $news->delete();
    }

    public function getNewsInThePeriodWithCategoryAndSource(GetNewsWithParamRequest $request)
    {
        $validatedData = $request->validated();

        $query = News::where('source_id', $validatedData['source_id'])
            ->where('category_id', $validatedData['category_id']);

        if (isset($validatedData['dateFrom'])) {
            $query->where('publication_date', '>=', $validatedData['dateFrom']);
        }
        if (isset($validatedData['dateTo'])) {
            $query->where('publication_date', '<=', $validatedData['dateTo']);
        }

        $news = $query->orderBy('publication_date', 'desc')->get();

        return $news;
    }


}
