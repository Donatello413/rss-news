<?php

namespace App\Services;

use App\Models\Source;
use App\Request\GetSourceWithParamRequest;
use App\Request\SourceRequest;

class SourceServiceImplementation
{
    public function index()
    {
        return Source::all();
    }

    public function store(SourceRequest $request)
    {
        $validatedData = $request->validated();

        return Source::create($validatedData);
    }

    public function show(Source $source)
    {
        return Source::query()->findOrFail($source->id);
    }

    public function update(SourceRequest $request, Source $source)
    {
        $validatedData = $request->validated();

        return $source->update($validatedData);
    }

    public function destroy(Source $source)
    {
        return $source->delete();
    }

    public function getSourceWithCountNewsInThePeriod(GetSourceWithParamRequest $request)
    {
        $validatedData = $request->validated();

        $query = Source::withCount('news');

        if (isset($validatedData['dateFrom'])) {
            $query->where('created_at', '>=', $validatedData['dateFrom']);
        }
        if (isset($validatedData['dateTo'])) {
            $query->where('created_at', '<=', $validatedData['dateTo']);
        }

        $sources = $query->orderBy('created_at', 'desc')->get();

        return $sources;
    }

}
