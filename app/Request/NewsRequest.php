<?php

namespace App\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NewsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'link' => 'required|string|url|max:255',
            'publication_date' => 'required|date',
            'source_id' => ['required', 'int', Rule::exists('sources', 'id')],
            'category_id' => ['required', 'int', Rule::exists('categories', 'id')],
            'image_url' => 'nullable|string|url|max:255',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
