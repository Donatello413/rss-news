<?php

namespace App\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GetNewsWithParamRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'dateFrom' => 'date|date_format:Y-m-d|before_or_equal:dateTo',
            'dateTo' => 'date|date_format:Y-m-d|before_or_equal:today',
            'source_id' => ['required', 'int', Rule::exists('sources', 'id')],
            'category_id' => ['required', 'int', Rule::exists('categories', 'id')],
        ];

    }

    public function authorize(): bool
    {
        return true;
    }
}
