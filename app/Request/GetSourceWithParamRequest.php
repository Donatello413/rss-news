<?php

namespace App\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GetSourceWithParamRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'dateFrom' => 'date|date_format:Y-m-d|before_or_equal:dateTo',
            'dateTo' => 'date|date_format:Y-m-d|before_or_equal:today',
        ];

    }

    public function authorize(): bool
    {
        return true;
    }
}
