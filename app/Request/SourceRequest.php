<?php

namespace App\Request;

use Illuminate\Foundation\Http\FormRequest;

class SourceRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'url' => 'required|url|max:255',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
