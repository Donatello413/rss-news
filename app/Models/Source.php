<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property-read int $id
 * @property string $title
 * @property string $url
 * @property-read Collection<News> $news
 */
class Source extends Model
{
    protected $table = 'sources';

    protected $fillable = [
        'title',
        'url'
    ];

    public function news(): HasMany
    {
        return $this->hasMany(News::class);
    }
}
