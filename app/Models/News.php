<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property-read int $id
 * @property string $title
 * @property string $description
 * @property string $link
 * @property string $publication_date
 * @property int $source_id
 * @property int $category_id
 * @property string $image_url
 * @property-read Collection<Source> $source
 * @property-read Collection<Category> $category
 */
class News extends Model
{
    protected $table = 'news';

    protected $fillable = [
        'title',
        'description',
        'link',
        'publication_date',
        'source_id',
        'category_id',
        'image_url'
    ];

    public function source(): BelongsTo
    {
        return $this->belongsTo(Source::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
