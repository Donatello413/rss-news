<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->string('link');
            $table->dateTime('publication_date');
            $table->foreignId('source_id')->constrained('sources');
            $table->foreignId('category_id')->constrained('categories');;
            $table->string('image_url')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('news');
    }
};
