<?php

use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\NewsController;
use App\Http\Controllers\Api\SourceController;
use Illuminate\Support\Facades\Route;

Route::apiResource('sources', SourceController::class);

Route::get('/sources-with-param', [SourceController::class, 'getSourceWithCountNewsInThePeriod']);

Route::apiResource('categories', CategoryController::class);

Route::apiResource('news', NewsController::class);

Route::get('/news-with-param', [NewsController::class, 'getNewsInThePeriodWithCategoryAndSource']);
